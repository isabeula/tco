from django.contrib import admin
from django.urls import path,include

urlpatterns = [
    path('pessoa/', include('pessoa.urls')),
    path('admin/', admin.site.urls),
#  path('estado/', include('estado.urls')),
]
