from django.db import models

# Create your models here.

class EstadoModel(models.Model):
    estado_id = models.AutoField(primary_key=True)
    estado_nome = models.CharField(
        max_length=15,
        null=False,
        blank=False,
        verbose_name="Nome")
    estado_sigla = models.CharField(
        max_length=15,
        null=False,
        blank=False,
        verbose_name="Sigla")

    class Meta:
        ordering = ['estado_nome']
        verbose_name = 'estado'
        verbose_name_plural = 'Estados'

    def __str__(self):
        return self.estado_sigla


class CidadeModel(models.Model):
    cidade_id = models.AutoField(primary_key=True)
    cidade_nome = models.CharField(
        max_length=15,
        null=False,
        blank=False,
        verbose_name="Cidade")
    estado = models.ForeignKey(EstadoModel, on_delete=models.CASCADE, null=True,blank=True)
