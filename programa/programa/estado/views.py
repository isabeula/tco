from django.shortcuts import render, redirect, get_object_or_404
#from.models import CidadeForm
from.forms import EstadoForm
from.models import EstadoModel
from.models import CidadeModel

# Create your views here.
#
# def update_pessoa(request, id):
#     pessoa = get_object_or_404(PessoaModel, pessoa_id=id)
#     form = PessoaForm(request.POST or None, instance=pessoa)
#     if form.is_valid():
#         form.save()
#         return redirect('list_pessoa')
#     context = {
#         'form':form
#     }
#     return render(request,'novo2.html', context)
#
#
# def delete_pessoa(request, id):
#     pessoa = get_object_or_404(PessoaModel, pessoa_id=id)
#     form = PessoaForm(request.POST or None, instance=pessoa)
#     if request.method == 'POST':
#         form.delete()
#         return redirect('list_pessoa')
#     context = {
#         'form':form
#     }
#     return render(request,'delete.html', context)


def list_estado(request):
    form = EstadoModel.objects.all()
    context = {
        'form':form,
    }
    return render(request, 'listarEstado.html', context)


def insert_estado(request):
    form = EstadoForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('list_estado')
    context = {
        'form':form
    }
    return render(request, 'novoEstado.html', context)