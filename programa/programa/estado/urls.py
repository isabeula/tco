from django.urls import path
from . import views

urlpatterns = [
    ##               nome funcao                chamada interna
    path('inserirEstado/', views.insert_tel, name="insert_estado"),
    path('listarEstado/', views.list_tel, name="list_estado"),
]
