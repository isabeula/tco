from django import forms
from .models import EstadoModel
from .models import CidadeModel

class EstadoForm(forms.ModelForm):
    estado_nome = forms.CharField(label="Nome")
    estado_sigla = forms.CharField(label="Sigla")

    class Meta:
        model = EstadoModel
        fields = [
            'estado_nome',
            'estado_sigla'
        ]

class CidadeForm(forms.ModelForm):
    cidade_nome = forms.CharField(label="Cidade")
    cidade_estado = forms.ModelChoiceField(queryset=EstadoModel.objects.all(), label="estado")

    class Meta:
        model = CidadeModel
        fields = [
            'cidade_nome',
            'cidade_estado'
        ]