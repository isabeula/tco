from django.shortcuts import render, redirect, get_object_or_404
from.forms import PessoaForm
from.forms import ContatoForm
from.forms import EstadoForm
from.forms import CidadeForm
from.models import PessoaModel
from.models import EstadoModel
from.models import ContatoModel
from.models import CidadeModel

# Create your views here.
def list_pessoa(request):
    form = PessoaModel.objects.all()
    context = {
        'form':form,
    }
    return render(request, 'listar.html', context)


def insert_pessoa(request):
    form = PessoaForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('list_pessoa')
    context = {
        'form':form
    }
    return render(request, 'novo3.html', context)


def update_pessoa(request, id):
    pessoa = get_object_or_404(PessoaModel, pessoa_id=id)
    form = PessoaForm(request.POST or None, instance=pessoa)
    if form.is_valid():
        form.save()
        return redirect('list_pessoa')
    context = {
        'form':form
    }
    return render(request,'novo2.html', context)


def delete_pessoa(request, id):
    pessoa = get_object_or_404(PessoaModel, pessoa_id=id)
    form = PessoaForm(request.POST or None, instance=pessoa)
    if request.method == 'POST':
        form.delete()
        return redirect('list_pessoa')
    context = {
        'form':form
    }
    return render(request,'delete.html', context)

def list_tel(request):
    form = ContatoModel.objects.all()
    context = {
        'form':form,
    }
    return render(request, 'listarTel.html', context)


def insert_tel(request):
    form = ContatoForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('list_tel')
    context = {
        'form':form
    }
    return render(request, 'novoTel.html', context)

######     ESTADO

def list_estado(request):
    form = EstadoModel.objects.all()
    context = {
        'form':form,
    }
    return render(request, 'listarEstado.html', context)


def insert_estado(request):
    form = EstadoForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('list_estado')
    context = {
        'form':form
    }
    return render(request, 'novoEstado.html', context)

def update_estado(request, id):
    estado = get_object_or_404(EstadoModel, estado_id=id)
    form = EstadoForm(request.POST or None, instance=estado)
    if form.is_valid():
        form.save()
        return redirect('list_estado')
    context = {
        'form':form
    }
    return render(request,'novoEstado.html', context)


def delete_estado(request, id):
    estado = get_object_or_404(EstadoModel, estado_id=id)
    form = EstadoForm(request.POST or None, instance=estado)
    if request.method == 'POST':
        form.delete()
        return redirect('list_estado')
    context = {
        'form':form
    }
    return render(request,'delete.html', context)

######     CIDADE

def list_cidade(request):
    form = CidadeModel.objects.all()
    context = {
        'form':form,
    }
    return render(request, 'listarCidade.html', context)


def insert_cidade(request):
    form = CidadeForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('list_cidade')
    context = {
        'form':form
    }
    return render(request, 'novoCidade.html', context)

def update_cidade(request, id):
    cidade = get_object_or_404(CidadeModel, cidade_id=id)
    form = CidadeForm(request.POST or None, instance=cidade)
    if form.is_valid():
        form.save()
        return redirect('list_cidade')
    context = {
        'form':form
    }
    return render(request,'novoCidade.html', context)


def delete_cidade(request, id):
    cidade = get_object_or_404(CidadeModel, cidade_id=id)
    form = CidadeForm(request.POST or None, instance=cidade)
    if request.method == 'POST':
        form.delete()
        return redirect('list_cidade')
    context = {
        'form':form
    }
    return render(request,'delete.html', context)