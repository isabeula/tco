from django import forms
from .models import PessoaModel
from .models import ContatoModel
from .models import EstadoModel
from .models import CidadeModel

class PessoaForm(forms.ModelForm):
    pessoa_nome = forms.CharField(required=True, label="Nome")
    pessoa_logradouro = forms.CharField(required=True,label="Logradouro")
    pessoa_numero = forms.CharField(required=True,label="Número")

    class Meta:
        model = PessoaModel
        fields = [
            'pessoa_nome',
            'pessoa_logradouro',
            'pessoa_numero',
        ]



class ContatoForm(forms.ModelForm):
    contato_telefone = forms.CharField(label="Telefone")
    contato_pessoa = forms.ModelChoiceField(queryset=PessoaModel.objects.all(), label="Contato")

    class Meta:
        model = ContatoModel
        fields = [
            'contato_telefone',
            'contato_pessoa'
        ]

class EstadoForm(forms.ModelForm):
    estado_nome = forms.CharField(label="Nome")
    estado_sigla = forms.CharField(label="Sigla")

    class Meta:
        model = EstadoModel
        fields = [
            'estado_nome',
            'estado_sigla'
        ]

class CidadeForm(forms.ModelForm):
    cidade_nome = forms.CharField(label="Cidade")
    cidade_estado = forms.ModelChoiceField(queryset=EstadoModel.objects.all(), label="estado")

    class Meta:
        model = CidadeModel
        fields = [
            'cidade_nome',
            'cidade_estado'
        ]