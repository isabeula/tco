from django.urls import path
from . import views

urlpatterns = [
    ##               nome funcao                chamada interna
    path('incluir', views.insert_pessoa, name="insert_pessoa"),
    path('alterar/<int:id>', views.update_pessoa, name="update_pessoa"),
    path('excluir/<int:id>', views.delete_pessoa, name="delete_pessoa"),
    path('listar', views.list_pessoa, name="list_pessoa"),
    path('inserirTel/', views.insert_tel, name="insert_tel"),
    path('listarTel/', views.list_tel, name="list_tel"),

    #ESTADO
    path('inserirEstado/', views.insert_estado, name="insert_estado"),
    path('listarEstado/', views.list_estado, name="list_estado"),
    path('alterarEstado/<int:id>', views.update_estado, name="update_estado"),
    path('excluirEstado/<int:id>', views.delete_estado, name="delete_estado"),

    #CIDADE
    path('inserirCidade/', views.insert_cidade, name="insert_cidade"),
    path('listarCidade/', views.list_cidade, name="list_cidade"),
    path('alterarCidade/<int:id>', views.update_cidade, name="update_cidade"),
    path('excluirCidade/<int:id>', views.delete_cidade, name="delete_cidade"),
]
